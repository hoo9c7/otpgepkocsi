﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace OtpGepkocsi
{
    class MyRestApi
    {
        //
        // Properties.
        //

        public  string Url_OTP { get; set; }

        public string MySerial { get; set; }

        public string ResponseJson { get; set; }
        
        //
        // Methods.
        //

        public void GetJsonResponse()
        {
            WebClient client = new WebClient();

            client.BaseAddress = Url_OTP;

            WebRequest request = WebRequest.Create(Url_OTP);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            ResponseJson = response.ToString();
        }
    }
}
